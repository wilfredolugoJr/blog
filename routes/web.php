<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::group(['prefix'=>'admin'], function ()
{
    Route::resource('users','UsersController');
    Route::get('users/{id}/destroy',[
        'uses'=>'UsersController@destroy',
        'as'=>'users.destroy'
    ]);
    Route::resource('categories','CategoriesController')->middleware('auth');
    Route::get('categories/{id}/destroy',[
        'uses'=>'CategoriesController@destroy',
        'as'=>'categories.destroy'
    ])->middleware('auth');


    Route::resource('tags','TagsController')->middleware('auth');
    Route::get('tags/{id}/destroy',[
        'uses'=>'TagsController@destroy',
        'as'=>'tags.destroy'
    ])->middleware('auth');


    Route::resource('articles','ArticlesController')->middleware('auth');

});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('articles/{id}/destroy',[
    'uses'=>'ArticlesController@destroy',
    'as'=>'articles.destroy'
])->middleware('auth');


Route::get('breweries', ['middleware' => 'cors', function()
{
    return \Response::json(\App\Brewery::with('beers', 'geocode')->paginate(10), 200);
}]);
