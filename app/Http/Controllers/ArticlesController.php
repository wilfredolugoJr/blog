<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Article;
use App\Category;
use App\Tag;
use App\Image;
use Laracasts\Flash\Flash;
use App\Http\Requests\ArticleRequest;


class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $articles=Article::Search($request->title)->orderBy('id','DESC')->paginate(5);

        $articles->each(function($articles)
        {
            $articles->category;
            $articles->user;
            $articles->images;
        });
        //dd($articles[0]->images);


        return view('admin.articles.index')->with('articles',$articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::orderBy('name','ASC')->pluck('name','id');
        $tags=Tag::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('admin.articles.create')
            ->with('categories',$categories)
            ->with('tags',$tags)
        ;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {

        if($request->file('image'))
        {
            $file = $request->file('image');
            $name = 'blog_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path(). '/images/articles/';
            $file -> move($path,$name);
        }

        $article=new Article($request->all());
        $article->user_id=\Auth::user()->id;
        $article->save();

        $article->tags()->sync($request->tags);


        $image=new Image();
        $image->nombre=$name;
        $image->article()->associate($article);
        $image->save();

        //dd($request->all());

        Flash::success('Se ha insertado correctamente el articulo '.$article->title);

        return redirect()->route('articles.index');









    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article=Article::find($id);
        $article->category();
        $categories=Category::orderBy('name','DESC')->pluck('name','id');
        $tags=Tag::orderBy('nombre','DESC')->pluck('nombre','id');

        $my_tags=$article->tags->pluck('id')->ToArray();
        //dd($my_tags);


            return view('admin.articles.edit')
                ->with('categories',$categories)
                ->with('tags',$tags)
                ->with('my_tags',$my_tags)
                ->with('article',$article);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $article=Article::find($id);
        if($request->file('image'))
        {
            $file = $request->file('image');
            $name = 'blog_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path(). '/images/articles/';
            $file -> move($path,$name);
            $image=Image::find($article->images[0]->id);
            $path = public_path() . '/images/articles/'.$image->nombre;
            if(file_exists($path))
            {
                unlink($path);
            }
            //$image->delete();
            $image->nombre=$name;
            $image->save();

        }
        



       
        $article->fill($request->all());
        $article->save();

        $article->tags()->sync($request->tags);

        

        Flash::success('Se ha editado correctamente el articulo '.$article->title);
        return redirect()->route('articles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article=Article::find($id);
        $image=Image::find($article->images[0]->id);
        $path = public_path() . '/images/articles/'.$image->nombre;
        if(file_exists($path))
        {
            unlink($path);
        }
        $image->delete();
        
        $article->delete();

        Flash::error('Se ha eliminado correctamente el articulo '.$article->title);
        return redirect()->route('articles.index');
    }
}
