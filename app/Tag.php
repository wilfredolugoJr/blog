<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tags";
    protected $fillable = ['nombre'];

    public function articles()
    {
        return $this->belongsToMany('App\Article')->withTimestamps();
    }
    public function scopeSearch($query,$nombre)
    {
        return $query->where('nombre','LIKE',"%$nombre%");
    }
}
