@extends('admin.template.main')

@section('title','Crear Usuario')

@section('content')







            {!! Form::open(['route'=>'users.store','method'=>'POST']) !!}

            <div class="form-group">
                {!! Form::label('name','Nombre') !!}
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre','required']) !!}
                @if ($errors->has('name'))
                    @foreach ($errors->get('name') as $message)
                        {{$message}}
                    @endforeach
                    @endif
            </div>

            <div class="form-group">
                {!! Form::label('email','Email') !!}
                {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'example@gmail.com','required']) !!}

            </div>

            <div class="form-group">
                {!! Form::label('password','Contraseña') !!}
                {!! Form::password('password',['class'=>'form-control','placeholder'=>'*******************','required']) !!}

            </div>

            <div class="form-group">
                {!! Form::label('type','Rol') !!}
                {!! Form::select('type',[''=>'Seleccione','member'=>'Miembro','admin'=>'Administrador'],null,['class'=>'form-control' ]) !!}

            </div>

            <div class="form-group">
                {!! Form::submit('Registrar',['class'=>'btn btn-primary' ]) !!}
                <a href="{{route('users.index')}}" class="btn btn-info">Regresar</a>
            </div>


            {!! Form::close() !!}




@endsection