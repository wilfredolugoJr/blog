@extends('admin.template.main')

@section('title','Lista de Usuarios')

@section('content')





            <a href="{{route('users.create')}}" class="btn btn-info">Registar nuevo usuario</a>
            {!! Form::open(['route'=>'users.index','method'=>'GET','class'=>'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar Usuario','aria-describedby'=>'search']) !!}
                <span class="input-group-addon" id="search">
            <span class="glyphicon glyphicon-search" aria-hidden="true">

            </span>
        </span>

            </div>

            {!! Form::close() !!}
            <hr>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                       <th>Id</th>
                       <th>Nombre</th>
                       <th>Email</th>
                       <th>Tipo</th>
                       <th>Acciones</th>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @if($user->type=="admin")
                                <span class="label label-danger">{{$user->type}}</span>
                            @else
                                <span class="label label-primary">{{$user->type}}</span>
                            @endif

                        </td>
                        <td>
                            <a href="{{route('users.edit',$user->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                            <a href="{{route('users.destroy',$user->id)}}"  onclick="return confirm('¿Seguro que deseas eliminarlo')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                        </td>

                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="text-align: center">
                    {!! $users->render() !!}
                </div>

            </div>
            <div style="border-bottom: 1px solid #eee;margin-top: 10px;">

            </div>





@endsection