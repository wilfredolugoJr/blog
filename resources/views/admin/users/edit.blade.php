@extends('admin.template.main')

@section('title','Editar Usuario ' .$user->name )

@section('content')


            {!! Form::open(['route'=>['users.update',$user->id],'method'=>'PUT','']) !!}

            <div class="form-group">
                {!! Form::label('name','Nombre') !!}
                {!! Form::text('name',$user->name,['class'=>'form-control','placeholder'=>'Nombre','required']) !!}

            </div>

            <div class="form-group">
                {!! Form::label('email','Email') !!}
                {!! Form::email('email',$user->email,['class'=>'form-control','placeholder'=>'example@gmail.com','required']) !!}

            </div>



            <div class="form-group">
                {!! Form::label('type','Rol') !!}
                {!! Form::select('type',['member'=>'Miembro','admin'=>'Administrador'],$user->type,['class'=>'form-control' ]) !!}

            </div>

            <div class="form-group">
                {!! Form::submit('Editar',['class'=>'btn btn-primary' ]) !!}
                <a href="{{route('users.index')}}" class="btn btn-info">Regresar</a>
            </div>


            {!! Form::close() !!}



@endsection