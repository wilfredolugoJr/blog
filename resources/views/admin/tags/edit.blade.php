@extends('admin.template.main')

@section('title','Editar Tag')

@section('content')





    {!! Form::open(['route'=>['tags.update',$tag->id],'method'=>'PUT']) !!}

    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',$tag->nombre,['class'=>'form-control','placeholder'=>'Nombre','required']) !!}

    </div>



    <div class="form-group">
        {!! Form::submit('Editar',['class'=>'btn btn-primary' ]) !!}
        <a href="{{route('tags.index')}}" class="btn btn-info">Regresar</a>

    </div>


    {!! Form::close() !!}




@endsection