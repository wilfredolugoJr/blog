@extends('admin.template.main')

@section('title','Crear Tags')

@section('content')





    {!! Form::open(['route'=>'tags.store','method'=>'POST']) !!}

    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Nombre','required']) !!}

    </div>



    <div class="form-group">
        {!! Form::submit('Registrar',['class'=>'btn btn-primary' ]) !!}
        <a href="{{route('tags.index')}}" class="btn btn-info">Regresar</a>

    </div>


    {!! Form::close() !!}




@endsection