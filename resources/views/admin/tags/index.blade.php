@extends('admin.template.main')

@section('title','Lista de Categorias')

@section('content')





    <a href="{{route('tags.create')}}" class="btn btn-info">Registar un nuevo tag</a>

    {!! Form::open(['route'=>'tags.index','method'=>'GET','class'=>'navbar-form pull-right']) !!}
    <div class="input-group">
        {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Buscar Tag','aria-describedby'=>'search']) !!}
        <span class="input-group-addon" id="search">
            <span class="glyphicon glyphicon-search" aria-hidden="true">

            </span>
        </span>

    </div>

    {!! Form::close() !!}
    <hr>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <th>Id</th>
            <th>Nombre</th>

            </thead>
            <tbody>
            @foreach($tags as $tag)
                <tr>
                    <td>{{$tag->id}}</td>
                    <td>{{$tag->nombre}}</td>

                    <td>
                        <a href="{{route('tags.edit',$tag->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                        <a href="{{route('tags.destroy',$tag->id)}}"  onclick="return confirm('¿Seguro que deseas eliminarlo')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div style="text-align: center">
            {!! $tags->render() !!}
        </div>
    </div>
    <div style="border-bottom: 1px solid #eee;margin-top: 10px;">

    </div>





@endsection