@extends('admin.template.main')

@section('title','Editar Categoria')

@section('content')





    {!! Form::open(['route'=>['categories.update',$category->id],'method'=>'PUT']) !!}

    <div class="form-group">
        {!! Form::label('name','Nombre') !!}
        {!! Form::text('name',$category->name,['class'=>'form-control','placeholder'=>'Nombre','required']) !!}

    </div>



    <div class="form-group">
        {!! Form::submit('Editar',['class'=>'btn btn-primary' ]) !!}
        <a href="{{route('categories.index')}}" class="btn btn-info">Regresar</a>

    </div>


    {!! Form::close() !!}




@endsection