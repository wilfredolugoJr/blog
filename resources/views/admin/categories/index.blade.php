@extends('admin.template.main')

@section('title','Lista de Categorias')

@section('content')





    <a href="{{route('categories.create')}}" class="btn btn-info">Registar una nueva categoria</a>

    {!! Form::open(['route'=>'categories.index','method'=>'GET','class'=>'navbar-form pull-right']) !!}
    <div class="input-group">
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Buscar Tag','aria-describedby'=>'search']) !!}
        <span class="input-group-addon" id="search">
            <span class="glyphicon glyphicon-search" aria-hidden="true">

            </span>
        </span>

    </div>

    {!! Form::close() !!}
    <hr>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <th>Id</th>
            <th>Nombre</th>

            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>

                    <td>
                        <a href="{{route('categories.edit',$category->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                        <a href="{{route('categories.destroy',$category->id)}}"  onclick="return confirm('¿Seguro que deseas eliminarlo')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div style="text-align: center">
        {!! $categories->render() !!}
        </div>
        </div>
        <div style="border-bottom: 1px solid #eee;margin-top: 10px;">

        </div>





@endsection