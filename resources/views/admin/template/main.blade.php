<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title','default')|Documento</title>
    <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/chosen/chosen.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/ui/trumbowyg.css')}}">

</head>
<body>
<div class="container">


        @include('admin.template.nav')

    <div class="row">
        <section>
            <div class="panel col-sm-offset-2 col-sm-8 col-sx-4 panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@yield('title')</h3>
                </div>
                <div class="panel-body">

                    @include('flash::message')
                    @include('admin.template.errors')

                    @yield('content')

                </div>
            </div>

        </section>
    </div>




    <div class="footer">
        <div style="border-bottom: 1px solid #eee;margin-top: 30px;">

        </div>
        <div style="border-bottom: 1px solid #eee;margin-top: 10px;">
            <p>©2018</p>
        </div>

    </div>

</div>
<script src="{{asset('plugins/bootstrap/js/jquery.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/docs.min.js')}}"></script>
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('plugins/trumbowyg.js')}}"></script>
@yield('js')
</body>
</html>