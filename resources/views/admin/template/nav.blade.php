
<div class="container">
<div style="border-radius: 6px" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
           
        </div>
        <div class="navbar-collapse collapse">
            <ul style="padding-left: 55px" class="nav navbar-nav">
                <li class="active"><a href="{{route('welcome')}}">Inicio</a></li>

                <li><a href="{{route('users.index')}}">Usuarios</a></li>
                <li><a href="{{route('categories.index')}}">Categorias</a></li>
                <li><a href="{{route('tags.index')}}">Tags</a></li>
                <li><a href="{{route('articles.index')}}">Articulos</a></li>
                
                
            </ul>

            <ul style="float:right;" class="nav navbar-nav">
                @guest
                <li ><a href="{{route('login')}}">Login</a></li>
                <li><a href="{{route('register')}}">Registrar</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endguest
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
</div>
<div style="border-bottom: 1px solid #eee;margin-top: 60px;"></div>
<div style="margin-top: 10px" class="jumbotron">
  
    
</div>