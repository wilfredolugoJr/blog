@extends('admin.template.main')

@section('title','Crear Articulos')

@section('content')





    {!! Form::open(['route'=>'articles.store','method'=>'POST','files'=>true]) !!}

    <div class="form-group">
        {!! Form::label('title','Titulo') !!}
        {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Titulo','required']) !!}

    </div>
    <div class="form-group">
        {!! Form::label('category_id','Category') !!}
        {!! Form::select('category_id',$categories,null,['class'=>'form-control select-category','placeholder'=>"Selecciona una categoria...",'required']) !!}

    </div>
    <div class="form-group">
        {!! Form::label('content','Contenido') !!}
        {!! Form::textarea('content',null,['class'=>'form-control ','placeholder'=>'Contenido...','required']) !!}

    </div>

    <div class="form-group">
        {!! Form::label('tags','Tag') !!}
        {!! Form::select('tags[]',$tags,null,['class'=>'form-control select-tag','multiple','required']) !!}

    </div>

    <div class="form-group">
        {!! Form::label('image','Imagen') !!}
        {!! Form::file('image')!!}

    </div>

    <div class="form-group">
        {!! Form::submit('Registrar',['class'=>'btn btn-primary' ]) !!}
        <a href="{{route('articles.index')}}" class="btn btn-info">Regresar</a>

    </div>


    {!! Form::close() !!}







@endsection

@section('js')
    <script>
        $('.select-tag ').chosen(
                {
                    placeholder_text_multiple:'Seleccione un minimo de 3 tags',
                    max_selected_options:3,
                    no_results_text:'No se existen datos que mostrar'
                }
        );

        $('.select-category ').chosen(
                {
                    placeholder_text:'Seleccione una categoria',
                    no_results_text:'No se existen datos que mostrar'
                }
        );

        $('.text_area_content').trumbowyg();




    </script>
@endsection

