@extends('admin.template.main')

@section('title','Lista de Articulos')

@section('content')



    <a href="{{route('articles.create')}}" class="btn btn-info">Registar un nuevo articulo</a>

    {!! Form::open(['route'=>'articles.index','method'=>'GET','class'=>'navbar-form pull-right']) !!}
    <div class="input-group">
        {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Buscar Articulos','aria-describedby'=>'search']) !!}
        <span class="input-group-addon" id="search">
            <span class="glyphicon glyphicon-search" aria-hidden="true">

            </span>
        </span>

    </div>
    {!! Form::close() !!}
    <hr>


    <div class="table-responsive">
        <table class="table">
            <thead>
            <th>Id</th>
            <th>Image</th>
            <th>Title</th>
            <th>Category</th>
            <th>User</th>
            <th>Acciones</th>

            </thead>
            <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>{{  $article->id }}</td>
                    <td>@if($article->images!=null)
                            @foreach($article->images as $image)
                                <img style="width: 80px; height: 60px" src="{{ asset('images/articles/'.$image->nombre) }}">

                            @endforeach

                        @endif
                    </td>
                    <td>{{  $article->title }}</td>
                    <td>{{  $article->category->name }}</td>
                    <td>{{  $article->user->name }}</td>

                    <td>
                        <a href="{{route('articles.edit',$article->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                        <a href="{{route('articles.destroy',$article->id)}}"  onclick="return confirm('¿Seguro que deseas eliminarlo')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div style="text-align: center">
            {!! $articles->render() !!}
        </div>
    </div>
    <div style="border-bottom: 1px solid #eee;margin-top: 10px;">

    </div>





@endsection